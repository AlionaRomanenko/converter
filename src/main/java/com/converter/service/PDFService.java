package com.converter.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class PDFService<T> {

    private static final Logger LOGGER= LoggerFactory.getLogger(PDFService.class);

    public void convertEntityToPDF(T object, String fileName) throws FileNotFoundException, DocumentException, JsonProcessingException {
        Document document = new Document();
        LOGGER.info("Creation of file with name {} started",fileName+".pdf");
        PdfWriter.getInstance(document, new FileOutputStream(fileName+ ".pdf"));
        document.open();
        Font font = FontFactory.getFont(FontFactory.TIMES_ROMAN, 14);
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        String json = mapper.writeValueAsString(object);
        String strNew = json.replaceAll("([\"{}])", "");
        Paragraph paragraph = new Paragraph(strNew,font);
        document.add(paragraph);
        LOGGER.info("File with name {} was successfully created",fileName+".pdf");
        document.close();

    }
}
