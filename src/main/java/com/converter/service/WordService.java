package com.converter.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;

public class WordService <T> {

    private static final Logger LOGGER= LoggerFactory.getLogger(WordService.class);

    public void convertEntityToWord(T object, String fileName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        LOGGER.info("Creation of file with name {} started",fileName+".docx");
        String json = mapper.writeValueAsString(object);
        String str = json.replaceAll("([\"{}])", "");
        String strNew = str.replaceAll("(}$)", "");


        try (FileOutputStream outputStream = new FileOutputStream(fileName + ".docx");
            XWPFDocument document = new XWPFDocument()) {
            XWPFParagraph para1 = document.createParagraph();
            XWPFRun para1Run = para1.createRun();
            String[] strings = strNew.split(",");
            for (int i = 0; i < strings.length; i++) {
                para1Run.addBreak();
                if (strings[i].contains("}")) {
                    strings[i] = strings[i].replaceAll("(}])", ";");
                    para1Run.setText(strings[i]);
                } else para1Run.setText(strings[i] + ",");
            }
            LOGGER.info("File with name {} was successfully created",fileName+".docx");
            document.write(outputStream);
        }


    }
}
