package com.converter.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class XMLService <T> {

    private static final Logger LOGGER= LoggerFactory.getLogger(XMLService.class);

    public void convertEntityToXML(T object,String fileName) throws IOException {
        JacksonXmlModule xmlModule = new JacksonXmlModule();
        LOGGER.info("Creation of file with name {} started",fileName+".xml");
        xmlModule.setDefaultUseWrapper(false);
        ObjectMapper objectMapper = new XmlMapper(xmlModule);
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        String xml = objectMapper.writeValueAsString(object);
        File xmlOutput = new File(fileName+".xml");
        try (FileWriter fileWriter = new FileWriter(xmlOutput)) {
            LOGGER.info("File with name {} was successfully created",fileName+".xml");
            fileWriter.write(xml);
        }

    }
}
