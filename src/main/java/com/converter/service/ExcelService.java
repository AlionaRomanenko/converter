package com.converter.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ExcelService <T>{

    private static final Logger LOGGER= LoggerFactory.getLogger(ExcelService.class);

    public void convertEntityToExcel(T object,String fileName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        LOGGER.info("Creation of file with name {} started",fileName+".xlsx");
        String json = mapper.writeValueAsString(object);
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet();
        Map<String, Object> map = mapper.readValue(json, Map.class);
        int rowCount = 0;
        int cellCount = 1;
        int columnCount= 0;
        convertSimpleEntityToExcel(map,rowCount,cellCount,columnCount,sheet);

        FileOutputStream outputStream = new FileOutputStream(fileName+".xlsx");
        workbook.write(outputStream);
        LOGGER.info("File with name {} was successfully created",fileName+".xlsx");
        workbook.close();


    }

    public static int convertSimpleEntityToExcel(Map<String, Object> map,int rowCount,int cellCount,int columnCount,XSSFSheet sheet)  {
        for (String key1 : map.keySet()) {
            if (map.get(key1) instanceof Map) {
                LinkedHashMap<String, Object> map2 = (LinkedHashMap<String, Object>) map.get(key1);
                columnCount=convertSimpleEntityToExcel(map2, rowCount, cellCount, columnCount, sheet);
            }
            if (map.get(key1) instanceof List) {
                if (((List) map.get(key1)).get(0) instanceof Map || ((List) map.get(key1)).get(0) instanceof List) {
                    int listTypeCount = 1;
                    for (Object key4 : (List) map.get(key1)) {
                        LinkedHashMap<String, Object> hashMap = (LinkedHashMap<String, Object>) key4;
                        convertSimpleEntityToExcel(hashMap, rowCount, listTypeCount, columnCount, sheet);

                        listTypeCount++;
                    }
                    rowCount = listTypeCount;
                    listTypeCount = 1;
                }

            }
            if (!(map.get(key1) instanceof List) && !(map.get(key1) instanceof Map)) {
                if (sheet.getRow(rowCount) == null) {
                    sheet.createRow(rowCount);
                }
                if (sheet.getRow(cellCount) == null) {
                    sheet.createRow(cellCount);
                }
                Cell cell = sheet.getRow(0).createCell(columnCount);
                cell.setCellValue(key1);
                Cell cell1 = sheet.getRow(cellCount).createCell(columnCount);
                if (map.get(key1) instanceof String)
                    cell1.setCellValue((String) map.get(key1));
                if (map.get(key1) instanceof Integer)
                    cell1.setCellValue((Integer) map.get(key1));
                if (map.get(key1) instanceof Boolean)
                    cell1.setCellValue((Boolean) map.get(key1));
                if (map.get(key1) instanceof Double)
                    cell1.setCellValue((Double) map.get(key1));

                    columnCount++;

                }

            }
            return columnCount;
        }
    }
