package com.converter.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.opendevl.JFlat;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExcelServiceCSV<T> {

	private static final Logger LOGGER= LoggerFactory.getLogger(ExcelServiceCSV.class);

    public void convertEntityToExcelCSV(T object, String fileName) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		LOGGER.info("Creation of file with name {} started",fileName+".xlsx");
		String json = mapper.writeValueAsString(object);
		LOGGER.info("Creation of file with name {} started",fileName+".csv");
		JFlat flatMe = new JFlat(json);
		flatMe.json2Sheet().headerSeparator("_").write2csv(fileName+".csv");
		LOGGER.info("File with name {} was successfully created",fileName+".csv");
		LOGGER.info("Convertion of .csv file to .xlsx started ");
		List<String> list = new ArrayList<>();
		List<List<String>> complexList = new ArrayList<>();
		try (Stream<String> stream = Files.lines(Paths.get(fileName + ".csv"), StandardCharsets.UTF_8)) {
			list = stream
					.collect(Collectors.toList());
		}
		for (String s : list) {
				complexList.add(Arrays.asList(s.split(",")));
			}


		try (FileOutputStream outputStream = new FileOutputStream(fileName+".xlsx");
			XSSFWorkbook workbook = new XSSFWorkbook()) {
			XSSFSheet sheet = workbook.createSheet();
			int j = 0;
			for (List<String> stringList : complexList) {
				XSSFRow row = sheet.createRow(j);
				for (int i = 0; i < stringList.size(); i++) {
					XSSFCell cell = row.createCell(i);
					String data = stringList.get(i);
					data = data.replaceAll("\"", "");
					cell.setCellValue(data);
					}
					j++;
				}
			LOGGER.info("Convertion of .csv file to .xlsx successfully finished ");
			LOGGER.info("File with name {} was successfully created",fileName+".xlsx");
			workbook.write(outputStream);
		}

		}
	}

